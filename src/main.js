import 'normalize.css';
import './styles/index.css';

import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import injectTapEventPlugin from 'react-tap-event-plugin';
import configureStore from './store/configureStore.js';
import App from './App';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

let store = configureStore({});
injectTapEventPlugin();

let doRender = () => {
    const App = require('./App').default;
    render(
        (<MuiThemeProvider muiTheme={ getMuiTheme() }>
            <Provider store={store}>
                <App />
            </Provider>
        </MuiThemeProvider>),
        document.getElementById('root')
    );
};

if (module.hot) {
    module.hot.accept('./App', doRender);
}

doRender();
