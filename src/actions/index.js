export const TIMER_TICK = 'TIMER_TICK';

export const updateTick = (tick) => {
    return {
        type: TIMER_TICK,
        tick
    };
};
