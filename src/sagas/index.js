import { fork, put } from 'redux-saga/effects';
import { updateTick } from '../actions/';

export const delay = function (ms) {
    return new Promise(resolve => setTimeout(() => resolve(true), ms));
};

const HelloSagas = function* () {
    while (true) {
        yield delay(1000);
        yield put(updateTick(Date.now()));
    }
};

export default function* () {
    yield HelloSagas();
};
