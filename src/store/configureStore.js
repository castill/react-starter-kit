import {
    createStore,
    applyMiddleware,
    compose
} from 'redux';

import reducers from '../reducers';
import sagas from '../sagas';
import createSagaMiddleware from 'redux-saga';

export default function configureStore(initialState) {

    const sagaMiddleware = createSagaMiddleware();

    const enhancerList = [applyMiddleware(sagaMiddleware)];

    if (window.__REDUX_DEVTOOLS_EXTENSION__) {
        enhancerList.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }

    const enhancer = compose.apply(undefined, enhancerList);

    let store = createStore(
        reducers,
        initialState,
        enhancer
    );
    sagaMiddleware.run(sagas);

    // Hot reload reducers (requires Webpack or Browserify HMR to be enabled)
    if (module.hot) {
        module.hot.accept('../reducers', () => {
            store.replaceReducer(require('../reducers'));
        });
    }

    return store;
}
