export default class Hello {
    constructor () {
        console.log('Hello');
    }

    testReturnObject () {
        return {
            Hi: 'Test'
        };
    }

    testMock () {
        // should mock this
        this.testReturnObject();

        return [];
    }
}
