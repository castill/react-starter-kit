import React, { Component } from 'react';
import { connect } from 'react-redux';
import Paper from 'material-ui/Paper';

let App = ({tick}) => {
    return (
        <Paper className="clock">
            <h1>Hello React 15.6.1</h1>
            <h2>React, redux, redux-saga, es2015, material-ui</h2>
            <span>{new Date(tick).toString()}</span>
        </Paper>
    );
};

const mapStateToProps = (state) => {
    return state.Timer;
};

export default connect(mapStateToProps)(App);
