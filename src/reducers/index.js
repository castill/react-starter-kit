import { combineReducers } from 'redux';
import { TIMER_TICK } from '../actions/';

const Timer = (state = {tick: Date.now()}, action) => {
    switch (action.type) {
    case TIMER_TICK:
        return {
            ...state,
            tick: action.tick
        };
    default:
        return state;
    }
};

const reducers = combineReducers({
    Timer
});

export default reducers;
