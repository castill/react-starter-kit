import Hello from '../src/Hello.js';

describe('Hello Test', () => {
    it('should be true', () => {
        expect(Hello).toBeFunction();
    });
});

describe('Hell Object Test', function() {
    let instance;
    beforeEach(function() {
        instance = new Hello();
    });

    it('should return object', function() {
        expect(instance.testReturnObject()).toBeObject();
    });

    it('should mock function', () => {
        instance.testReturnObject = sinon.spy();
        expect(instance.testMock()).toBeArray();
        expect(instance.testReturnObject).toHaveBeenCalledOnce();
    });

});
