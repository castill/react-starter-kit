const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    devtool: 'cheap-module-source-map',
    entry: {
        'bundle': [
            // WebpackDevServer host and port
            'webpack-dev-server/client?http://localhost:8080',
            // "only" prevents reload on syntax errors
            'webpack/hot/only-dev-server',
            'react-hot-loader/patch',
            './src/main.js'
        ],
    },
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /.jsx?$/,
            use: [{
                loader: 'babel-loader',
                options: {
                    presets: ['es2015', 'react'],
                    plugins: [
                        'transform-object-rest-spread',
                        'transform-class-properties',
                        'react-hot-loader/babel'
                    ]
                }
            }],
            exclude: /node_modules/
        }, {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'postcss-loader',
                    options: {
                        plugins: [
                            require('postcss-import')({
                                addDependencyTo: webpack
                            }),
                            require('postcss-url')(),
                            require('postcss-cssnext')(),
                            require('postcss-browser-reporter')(),
                            require('postcss-reporter')()
                        ]
                    }
                }]
            })
        }]
    },
    plugins: [
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            options: {
                cssnext: {
                    browsers: 'last 2 versions'
                }
            }
        }),
        new ExtractTextPlugin({
            filename: '[name].css',
            disable: false,
            allChunks: false
        }),
        new HtmlWebpackPlugin({
            title: 'Starter Kit',
            template: 'index.html',
            baseUrl: '/'
        })
    ],
    devServer: {
        compress: true,
        disableHostCheck: true,
        historyApiFallback: true
    }
};
